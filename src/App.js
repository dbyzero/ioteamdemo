import React, { Component } from 'react';
import './style/css/App.css';

// Img
import exit from './style/img/cross.svg';
import add from './style/img/add.svg';
import sub from './style/img/sub.png';
import div from './style/img/div.png';
import mul from './style/img/mul.svg';

// Material UI
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";

const headers = new Headers();

const getHeaders = {
  method: 'GET',
  headers: headers,
  mode: 'cors',
  cache: 'default',
  'Content-Type': 'application/json',
};

let postHeaders = {
  method: 'POST',
  headers: headers,
  mode: 'cors',
  cache: 'default',
  'Content-Type': 'application/json',
};

let divideByZeroErrors = [
    "Do not try this at home.",
    "Mr Stark, I don't feel so good ...",
    "This operation successfully failed.",
    "WTF",
    "Remove Sys32 to solve that error.",
    "This error is unknown because the guy who wrote that part of the code quit a long time ago and he was like hella smart so I don't understand it. If you are that guy, please comeback",
    "Game Over.",
    "Internal bleeding.",
    "Stop doing that or i'll summon a blue screen.",
    "It works on IE 8, trust me.",
    "RUN !!",
    "Maybe Shenron knows the answer to that.",
    "Behind you !!",
];

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      operation: [
        { name: "Addition", img: add, selected: true, symbol: '+' },
        { name: "Soustraction", img: sub, selected: false, symbol: '-' },
        { name: "Division", img: div, selected: false, symbol: '/' },
        { name: "Multiplication", img: mul, selected: false, symbol: '*' },
      ],
      selectedOperation: '+',
      inputValue: 0,
      value: 1,
      isLoaded: false,
      item: '',
      divideByZero: false,
      divideByZeroText: '',
      error: false,
    }
  };

  componentDidMount() {
    this.getData()
  }

  getData = () => {
    fetch("http://localhost:5002/", getHeaders)
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({ isLoaded: this.fetchState(result)});
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  };

  fetchState = (history) => {
    const result = history.length > 0 ? history[history.length - 1].result : 0;
    this.setState({ value: result });
    return true;
  };

  handleValueChange = (e) => {this.setState({ inputValue: e.target.value })};

  selectItem = (index) => {
    let temp = { ...this.state };
    temp.operation.forEach(function(element) {
      element.selected = false;
    });
    temp.operation[index].selected = true;
    this.setState({ selectedOperation: temp.operation[index].symbol });
    this.setState({ operation: temp.operation });
  };

  history = () => {
    fetch("http://localhost:5002/", getHeaders)
      .then(res => res.json())
      .then(
        (result) => {
          console.log(result);
        },
        (error) => {
          console.log(error);
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  };

  formObject = () => {
    let resultObject = {};
    resultObject.result = 0;

    this.setState({ error: false, divideByZeroError: false });

    switch(this.state.selectedOperation) {
        case '+':
            resultObject.result = this.state.value + parseFloat(this.state.inputValue);
            break;
        case '-':
            resultObject.result = this.state.value - parseFloat(this.state.inputValue);
            break;
        case '/':
            resultObject.result = this.state.value / parseFloat(this.state.inputValue);
            break;
        case '*':
            resultObject.result = this.state.value * parseFloat(this.state.inputValue);
            break;
            
    }

    resultObject.previousValue = this.state.value;
    resultObject.inputValue = parseFloat(this.state.inputValue);
    resultObject.operation = this.state.selectedOperation;

    if (this.state.selectedOperation === '/' && parseFloat(this.state.inputValue) === 0) {
      this.setState({ divideByZeroError: true });
      this.setState({inputValue: 0});
      return 'Z-ERR';
    }
    else if (this.state.inputValue === '') {
      this.setState({ error: true });
      return 'ERR';
    }
    else {
      this.setState({inputValue: ''});
      return resultObject;
    }
  };

  updateValue = () => {
    let result = this.formObject();
    if (result === 'Z-ERR')
      this.setState({divideByZeroError: divideByZeroErrors[Math.floor(Math.random() * 14)]});
    else {
      postHeaders.body = JSON.stringify(result);
      fetch('http://localhost:5002/', postHeaders)
        .then(res => res.json())
        .then(
          () => {
            this.getData();
          },
          (error) => {
            this.setState({
              isLoaded: true,
              error
            });
          }
        )
    }
  };

  reset = () => {
    this.setState({ value: 0 });
  };

  render() {
    return (
      <div className="App">
        <div className="counter-container">
          <img className="exit" src={exit} alt="exit" />
          <div className="trace"><div className="content" /></div>
          {
           this.state.isLoaded ?
             <div className="counter-content">
               <div className="head">
                 <span onClick={ this.reset }>RESET</span>
                 <p onClick={ this.history }>ACCEDER A L'HISTORIQUE</p>
               </div>
               <div className="result">
                 <p>{ this.state.value }</p>
               </div>
               <div className="operation">
                 {
                   this.state.operation.map((item, index) => (
                       <div key={index} className={"op" + (item.selected ? ' selected' : '')} onClick={() => this.selectItem(index) }>
                         <p>{item.name}</p>
                         <img src={item.img} alt={item.name} />
                       </div>
                   ))
                 }
               </div>
               <div className="input-container">
                 <form>
                   <Grid container justify="center">
                     <TextField
                         variant="outlined"
                         value={ this.state.inputValue }
                         onChange={ this.handleValueChange }
                         margin="dense"
                         id="value-input"
                         label="Valeur"
                         fullWidth
                         type="number"
                     />
                   </Grid>
                   {
                     this.state.divideByZeroError ?
                       <div className="error-container">
                         <p>{ this.state.divideByZeroError }</p>
                       </div> : ''
                   }
                   {
                     this.state.error ?
                       <div className="error-container">
                         <p>La valeur d'entrée est vide !</p>
                       </div> : ''
                   }
                   <div className="custom-button">
                     <p onClick={ this.updateValue }>VALIDER</p>
                   </div>
                 </form>
               </div>
             </div> : 'LOADING'
          }
        </div>
      </div>
    );
  }
}

export default App;
